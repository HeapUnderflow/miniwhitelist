use adapter::minecraft::whitelist::WhitelistClient;
use rocket::{
    response::{NamedFile, Redirect},
};
use rocket_contrib::Template;
use std::{
    io,
    path::{Path, PathBuf},
};
use adapter;
use mcapi;
use serde_json;
use ::MCADAPTER;

#[derive(Serialize)]
pub struct TemplateContext {
    pub pack:      String,
    pub online:    bool,
    pub whitelist: adapter::minecraft::whitelist::CompoundWhitelist,
    pub state:     Option<mcapi::StatusResult>,
    pub raw_data:  Option<String>,
}

#[get("/<pack>")]
pub fn index(pack: String) -> io::Result<Template> {
    match MCADAPTER.get_state() {
        Ok(s) => {
            let mut data = WhitelistClient::load_compound_whitelist(
                &PathBuf::from(&pack),
                &s.data.players.sample.clone().unwrap_or_else(|| Vec::new()),
            )?;

            let raw = match serde_json::to_string_pretty(&s) {
                Ok(o) => o,
                Err(e) => return Err(io::Error::new(io::ErrorKind::Other, e)),
            };

            data.sort_by(|left, right| left.name.cmp(&right.name));
            Ok(Template::render(
                "page/index",
                TemplateContext {
                    pack,
                    online: true,
                    whitelist: data,
                    state: Some(s),
                    raw_data: Some(raw),
                },
            ))
        }
        Err(e) => match e.kind() {
            io::ErrorKind::TimedOut
            | io::ErrorKind::ConnectionRefused
            | io::ErrorKind::ConnectionReset => {
                let mut data =
                    WhitelistClient::load_compound_whitelist(&PathBuf::from(&pack), &Vec::new())?;
                data.sort_by(|left, right| left.name.cmp(&right.name));
                Ok(Template::render(
                    "page/index",
                    TemplateContext {
                        pack,
                        online: false,
                        whitelist: data,
                        state: None,
                        raw_data: None,
                    },
                ))
            }
            _ => return Err(e),
        },
    }
}

#[get("/index.html")]
pub fn redir_to_pindex() -> Redirect { Redirect::to("/") }

#[get("/favicon.ico")]
pub fn redir_to_favicon() -> Redirect { Redirect::to("/static/favicon.ico") }

#[get("/static/<f..>")]
pub fn serve_static(f: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static").join(f)).ok()
}