use mcapi::pinger::Player;
use serde_json;
use std::{fs, io, path::PathBuf};

pub type Whitelist = Vec<WhitelistUser>;

#[derive(Debug, Serialize, Deserialize)]
pub struct WhitelistUser {
    pub name: String,
    pub uuid: String,
}

pub type Oplist = Vec<OplistUser>;

#[derive(Debug, Serialize, Deserialize)]
pub struct OplistUser {
    pub name:  String,
    pub uuid:  String,
    pub level: u32,
}

pub type CompoundWhitelist = Vec<CompoundWhitelistUser>;

#[derive(Debug, Serialize, Deserialize)]
pub struct CompoundWhitelistUser {
    pub name:   String,
    pub uuid:   String,
    pub op:     bool,
    pub online: bool,
}

pub struct WhitelistClient;

impl WhitelistClient {
    pub fn load_oplist(mchome: &PathBuf) -> io::Result<Oplist> {
        let f = fs::read_to_string(mchome.join("ops.json"))?;
        let jvec: Oplist = serde_json::from_str(&f)?;
        Ok(jvec)
    }

    pub fn load_whitelist(mchome: &PathBuf) -> io::Result<Whitelist> {
        let f = fs::read_to_string(mchome.join("whitelist.json"))?;
        let jvec: Whitelist = serde_json::from_str(&f)?;
        Ok(jvec)
    }

    pub fn load_compound_whitelist(
        mchome: &PathBuf,
        po: &[Player],
    ) -> io::Result<CompoundWhitelist> {
        let wl = Self::load_whitelist(&mchome)?;
        let op = Self::load_oplist(&mchome)?;

        let mut list = Vec::new();
        for w in wl {
            let isop = op.iter().any(|o| o.uuid == w.uuid);
            let isonl = po.iter().any(|p| p.uuid == w.uuid);
            let nv = CompoundWhitelistUser {
                name:   w.name,
                uuid:   w.uuid,
                op:     isop,
                online: isonl,
            };

            list.push(nv);
        }

        Ok(list)
    }
}
