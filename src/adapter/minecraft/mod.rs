pub mod whitelist;

use parking_lot::{Mutex, RwLock};
use serde_json;
use std::{
    fs::{self, File},
    io::{self, Write},
    time::{Duration, Instant},
};
use toml;

use mcapi::{self, MinecraftServer, StatusResult};

pub type MinecraftServerData = MinecraftServer;
pub struct MinecraftServerAdapter {
    server:      Mutex<MinecraftServer>,
    state:       RwLock<StatusResult>,
    last_access: RwLock<Instant>,
    initialized: RwLock<bool>,
    _config:     Config,
}

impl MinecraftServerAdapter {
    pub fn run_from_file_config(conf: &str) -> io::Result<MinecraftServerAdapter> {
        let config = Self::_read_config(conf)?;

        let mut ns = MinecraftServer::new(
            config.minecraft_server.host.clone(),
            config.minecraft_server.port.clone(),
        );

        let (init_state, rd) = match ns.status() {
            Ok(o) => (o, true),
            Err(_) => (
                StatusResult {
                    latency: 0.0,
                    data:    mcapi::pinger::PingResponse {
                        favicon:     None,
                        players:     mcapi::pinger::Players {
                            max:    0,
                            online: 0,
                            sample: None,
                        },
                        description: serde_json::Value::String(String::new()),
                        modinfo:     None,
                        version:     mcapi::pinger::Version {
                            name:     String::new(),
                            protocol: 0,
                        },
                    },
                },
                false,
            ),
        };

        Ok(MinecraftServerAdapter {
            server:      Mutex::new(ns),
            state:       RwLock::new(init_state),
            last_access: RwLock::new(Instant::now()),
            initialized: RwLock::new(rd),
            _config:     config,
        })
    }

    pub fn get_state(&self) -> io::Result<StatusResult> {
        if self.last_access.read().elapsed() > Duration::new(5 * 60, 0) {
            let mut svr = self.server.lock();
            let mut state = self.state.write();
            let mut last_access = self.last_access.write();
            let init = { *self.initialized.read() };

            *state = svr.status()?;
            *last_access = Instant::now();

            if !init {
                let mut initialized = self.initialized.write();
                *initialized = true;
            }
        } else if !*self.initialized.read() {
            return Err(io::Error::new(
                io::ErrorKind::ConnectionRefused,
                String::from("Server connectivity unknown"),
            ));
        }

        Ok((*self.state.read()).clone())
    }

    fn _read_config(c: &str) -> io::Result<Config> {
        let f = match fs::read_to_string(c) {
            Ok(f) => f,
            Err(e) => {
                println!("Config not found, dumping default config...");
                let newcfg = match toml::to_string(&Config::default()) {
                    Ok(o) => o,
                    Err(e) => return Err(toml_ser_to_io_err(e)),
                };

                let mut nf = File::create(c)?;
                nf.write(&newcfg.into_bytes())?;
                return Err(e);
            }
        };

        match toml::from_str(&f) {
            Ok(o) => Ok(o),
            Err(e) => Err(toml_de_to_io_err(e)),
        }
    }
}

fn toml_ser_to_io_err(err: toml::ser::Error) -> io::Error {
    io::Error::new(io::ErrorKind::Other, err)
}

fn toml_de_to_io_err(err: toml::de::Error) -> io::Error {
    io::Error::new(io::ErrorKind::Other, err)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub minecraft_server: MinecraftServerConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MinecraftServerConfig {
    pub host: String,
    pub port: u16,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            minecraft_server: MinecraftServerConfig::default(),
        }
    }
}

impl Default for MinecraftServerConfig {
    fn default() -> Self {
        Self {
            host: String::new(),
            port: 25565,
        }
    }
}
