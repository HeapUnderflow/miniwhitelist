// Oauth auth URI
// https://discordapp.com/api/oauth2/authorize?client_id=449453045302951947&redirect_uri=https%3A%2F%2Fshiro.thesinglebyte.com%2Fmc%2Fdiscord%2Fauth&response_type=code&scope=identify

// {
//     id: String,
//     username: String,
//     discriminator: String,
//     avatar: ?String,
//     bot?: bool,
//     mfa_enabled?: bool,
//     verified?: bool,
//     email?: String // Needs `email` instead of `identify` scope.
// }

use oauth_client;

