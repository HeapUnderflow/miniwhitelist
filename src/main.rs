#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate mcapi;
extern crate parking_lot;
extern crate rand;
extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
extern crate toml;
extern crate oauth_client;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate lazy_static;

pub mod adapter;
pub mod pages;

use rocket::Request;
use rocket_contrib::Template;
use pages::{root, discord_api};

lazy_static! {
    pub static ref MCADAPTER: adapter::minecraft::MinecraftServerAdapter =
        adapter::minecraft::MinecraftServerAdapter::run_from_file_config("conf.toml")
            .expect("Unable to initialize MC-Adapter");
}

#[error(404)]
fn not_found(req: &Request) -> Template {
    let mut map = std::collections::HashMap::new();
    map.insert("path", req.uri().as_str());
    Template::render("error/404", &map)
}

#[error(500)]
fn internal_error(req: &Request) -> Template  {
    let mut map = std::collections::HashMap::new();
    map.insert("path", req.uri().as_str());
    Template::render("error/500", &map)
}

fn main() {
    rocket::ignite()
        // .mount("/", routes![index, redir_to_pindex, serve_static, whitelist, status])
        .mount("/", routes![root::index, root::redir_to_pindex, root::serve_static, root::redir_to_favicon])
        .mount("/api", routes![discord_api::discord_auth_stage1, discord_api::discord_auth_stage2])
        .attach(Template::fairing())
        .catch(errors![not_found])
        .launch();
}
